import sys
import os
import json
from rich.console import Console
from rich.table import Table
from PyQt5 import QtWidgets, QtGui, QtCore
import matplotlib.pyplot as plt

from exam_view import ExamView

# Class is for UI stuff printing structures and create QT window


class XLearningLangView:
    def __init__(self):
        self.console = Console()

    # Printing single word with possibly multiple translation
    def printSingleWordWithTrans(self, wordsTran: dict, src_lang: str, dst_lang: str):
        keys = list(wordsTran.keys())

        str_to_print = "[bold cyan]"+keys[1]+"[/bold cyan]"+" : "
        for x in wordsTran[keys[1]]:
            for key in x:
                if x[key] == dst_lang:
                    str_to_print += "[bold yellow]"+key+"[/bold yellow], "

        self.console.print(str_to_print, style="red")

    # create test window for user to test his knowledge
    def makeExam(self, words: list, src_lang: str, dst_lang: str):
        testWindow = ExamView(words, src_lang, dst_lang)
        testWindow.runExam()
        return testWindow.points_scored

    # printing single word with possibly multiple translation with pyqt
    def printRandomTranslationWithGUI(self, wordsTran: dict, src_lang: str, dst_lang: str):
        keys = list(wordsTran.keys())

        dst_words = ""
        for x in wordsTran[keys[1]]:
            for key in x:
                if x[key] == dst_lang:
                    dst_words += key+", "

        self.__setupQtWindowForRandomWord(
            src_lang + ': ' + keys[1], dst_lang + ': ' + dst_words[:-2])

    # exit program
    def __exitWindow(self):
        sys.exit(0)

    # settup qt window with .qtwincofig.json file
    def __setupQtWindowForRandomWord(self, srcText, dstText):
        with open(os.path.join(os.path.dirname(__file__), '.conf/.qtwinconfig.json')) as qtConfFile:
            qtConf = json.load(qtConfFile)

        # config basic qt window
        app = QtWidgets.QApplication([])
        window = QtWidgets.QWidget()
        window.setWindowTitle(qtConf.get('winTitle', ''))
        window.setGeometry(qtConf.get('winXPos', 600), qtConf.get(
            'winYPos', 400), qtConf.get('winWidth', 300), qtConf.get('winHeight', 150))

        # settup labels and buttons
        labelwelcome = QtWidgets.QLabel(window)
        labelsrc = QtWidgets.QLabel(window)
        labeldst = QtWidgets.QLabel(window)

        labelsrc = self.__setupQtElement(
            labelsrc, qtConf.get('srcXPos', 10), qtConf.get('srcYPos', 40), srcText, qtConf.get('labelFontSize', 14), qtConf.get('maxElementWidth', 280), QtCore.Qt.AlignCenter, 'color: '+qtConf.get('srcLangColor', '#0099ff'))
        labeldst = self.__setupQtElement(
            labeldst, qtConf.get('dstXPos', 10), qtConf.get('dstYPos', 65), dstText, qtConf.get('labelFontSize', 14), qtConf.get('maxElementWidth', 280), QtCore.Qt.AlignCenter, 'color: '+qtConf.get('dstLangColor', '#ff6666'))
        labelwelcome = self.__setupQtElement(
            labelwelcome, qtConf.get('welcomeXPos', 10), qtConf.get('welcomeYPos', 0), qtConf.get('welcomeMessage', 'Welcome!!!'), qtConf.get('welcomeFontSize', 24), qtConf.get('maxElementWidth', 280), QtCore.Qt.AlignCenter)

        button = QtWidgets.QPushButton(window)
        button = self.__setupQtElement(
            button, qtConf.get('buttonXPos', 10), qtConf.get('buttonYPos', 100), qtConf.get('dismissButtonMessage', 'Dismiss.'), qtConf.get('buttonFontSize', 18), qtConf.get('maxElementWidth', 280))
        button.clicked.connect(self.__exitWindow)

        window.show()
        return app.exec_()

    # settup for block text elements (like buttons and labels)
    def __setupQtElement(self, element, xPos, yPos, text, fontSize, fixedWidth, alignment=None, style=None):
        element.setFont(QtGui.QFont("Arial", fontSize, QtGui.QFont.Bold))
        element.setText(text)
        element.move(xPos, yPos)
        element.setFixedWidth(fixedWidth)
        if alignment != None:
            element.setAlignment(alignment)
        if style != None:
            element.setStyleSheet(style)
        return element

    # print list of words with translations to it
    def printListOfWordWithTrans(self, listWordsTran: list, src_lang: str, dst_lang: str):
        # configuration for rich.console
        table = Table(show_header=True, header_style="bold magenta")
        table.add_column("lp", style="dim")
        table.add_column(src_lang)
        table.add_column(dst_lang)

        i = 1
        for word in listWordsTran:
            keys = list(word.keys())
            translation = ""
            for x in word[keys[1]]:
                for key in x:
                    if x[key] == dst_lang:
                        translation += "[bold yellow]"+key+"[/bold yellow], "
            table.add_row(
                str(i) + '.', "[bold cyan]"+keys[1]+"[/bold cyan]", translation[:-2])
            i += 1

        self.console.print(table)

    # print using when something is translate
    def printTranslation(self, word: str, trans: str, from_lng: str, to_lng: str):
        self.console.print("[bold cyan]"+word + "[/bold cyan] => [bold yellow]" +
                           trans + "[/bold yellow] (" + from_lng + ' => ' + to_lng + ')', style='red')

    def plotLastTestResults(self, scores):
        if len(scores) == 0:
            print('No scores available!!!')
            return
        
        convert = lambda date, time, src, dst : (date[-5:]+'\n'+date[:4]+'\n'+time+'\n'+src+'=>'+dst)
        legend = []
        points = []
        
        for score in scores:
            points.append(score.get('points', 0))
            legend.append(convert(score.get('date', '0000-00-00-00-00'), score.get(
                'time', '00-00'), score.get('src', 'XX'), score.get('dst', 'XX')))

        print(points)
        print(legend)
        plt.subplots_adjust(bottom=0.2)
        plt.bar(legend[-8:], points[-8:])
        plt.xlabel('Days')
        plt.ylabel('Points')
        plt.gca().set_ylim([0,10])
        plt.title('Scored points in last 8 tests')
        plt.show()
