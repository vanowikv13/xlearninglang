import pymongo
import os
import json

from random import randint
from bson.objectid import ObjectId
from datetime import datetime

#class for communication with database
class DbHandler:
    def __init__(self):
        self.__setDbConnection()

    #get random element from database
    def getRandomWord(self, src_lang: str) -> tuple:
        list_words = self.getWordsList(src_lang)
        return list_words[randint(0, len(list_words)-1)]

    def getWordsList(self, src_lang: str) -> list:
        self.__setCollection(src_lang)
        list_words = []
        for x in self.__collection.find():
            tran = list(x.items())[1]
            transf = []

            #selection words from collection and swaping id value with it's name
            for y in tran[1]:
                id = ObjectId(list(y.items())[0][0])
                self.__setCollection(list(y.items())[0][1])
                t = self.__collection.find_one({"_id" : id})
                if t != None:
                    transf.append({list(t.items())[1][0] : list(y.items())[0][1]})
            #return only if words has translation to destination language
            if(len(transf) > 0):
                x[list(x.items())[1][0]] = transf
                list_words.append(x)
        return list_words

    #delete a word from list and links to this word from other words that are translation to it
    def deleteWord(self, src_lang: str, wordName: str):
        self.__setCollection(src_lang)
        word_in_col = self.__collection.find_one({wordName: {"$exists": True}})
        if word_in_col != None:
            self.__collection.delete_one({"_id": word_in_col['_id']})
            for x in word_in_col[wordName]:
                dict_to_arr = list(x.items())
                self.__setCollection(dict_to_arr[0][1])
                word_tran = self.__collection.find_one({"_id": ObjectId(dict_to_arr[0][0])})
                if word_tran != None:
                    self.__collection.update({
                        "_id": ObjectId(dict_to_arr[0][0])
                    }, {
                        "$pull": {list(word_tran.keys())[1]: {str(word_in_col["_id"]) : src_lang }}
                    })

    #insert word to database
    def __insertWord(self, word: str, lang: str, body: list = [], id=None, src_lang=None):
        word_in_col = self.__collection.find_one({word: {"$exists": True}})
        x = False

        if word_in_col == None:
            x = True
            inserted = self.__collection.insert_one({word: body})
        else:
            if id != None:
                self.__collection.update_one({'_id': word_in_col['_id']}, {
                                             '$addToSet': {word: {str(id): lang}}})

        return ObjectId(inserted.inserted_id) if x else word_in_col['_id']

    #insert word to database (specify structure)
    def insertWords(self, src_word: str, dst_word: str, src_lang: str, dst_lang: str):
        self.__setCollection(src_lang)
        src_id = self.__insertWord(src_word, src_lang)
        src_col = self.__collection

        self.__setCollection(dst_lang)
        dst_id = self.__insertWord(
            dst_word, dst_lang, [{str(src_id): src_lang}], src_id, src_lang)
        src_col.update_one({'_id': src_id}, {'$addToSet': {
                           src_word: {str(dst_id): dst_lang}}})

    #settup connection with database 
    def __setDbConnection(self):
        with open(os.path.join(os.path.dirname(__file__), '.conf/.dbconfig.json')) as config_files:
            self.__dbconfig = json.load(config_files)

        self.__client = pymongo.MongoClient(self.__dbconfig['mongodbAddress'])
        self.__db = self.__client[self.__dbconfig['xLearnLangDbName']]
        self.__collection = None

    #set collection we want to use
    def __setCollection(self, collectionName: str):
        if hasattr(self.__collection, 'name') and self.__collection.name == collectionName:
            return
        self.__collection = self.__db[collectionName]

    def insertTestScore(self, points :int, src_lang :str, dst_lang: str):
        self.__setCollection('test')
        date = datetime.now().strftime("%Y-%m-%d") #YYYY-MM-DD
        time = datetime.now().strftime("%H:%M:%S") #hh-mm
        self.__collection.insert_one({
            'date': date,
            'time': time,
            'points': points,
            'src': src_lang,
            'dst': dst_lang
        })


    def getTestScores(self):
        self.__setCollection('test')
        scores = []
        for x in self.__collection.find().sort('_id', 1).limit(8):
            scores.append(x)
        return scores