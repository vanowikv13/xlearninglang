from googletrans import Translator

#class for translation
class XTran:
    def __init__(self):
        self.translator = Translator()

    #translate from src language to destination language
    def translate(self, toTranslate :str, src_lng: str, dest_lng :str) -> str:
        translation = self.translator.translate(toTranslate, src=src_lng, dest=dest_lng)
        return translation.text