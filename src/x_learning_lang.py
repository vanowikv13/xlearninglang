import json
import sys
import os
import random

from db_handler import DbHandler
from xtran import XTran
from x_learning_lang_view import XLearningLangView

#Main class of a program
class XLearnLang:
    def __init__(self):
        #Object for communication with database
        self.dbHandler = DbHandler()

        #Object for translating
        self.translate = XTran()

        #Object for printing
        self.xLearnView = XLearningLangView()
        self.__setupDefaults()

    #running specific options on args
    def actionOnArgs(self, args, parser):
        word_translation = None
        try:
            if args.source_language:
                self.src_lang = args.source_language
            if args.destination_language:
                self.dst_lang = args.destination_language
            if args.insert:
                if self.src_lang == self.dst_lang:
                    raise Exception("Source language and destination language can't be the same on insertion")
                self.__insertWords(self.__stripSpaces(args.insert[0]), self.__stripSpaces(args.insert[1]))
            elif args.delete:
                self.__deleteWords(self.__stripSpaces(args.delete))
            elif args.random:
                self.xLearnView.printSingleWordWithTrans(self.__getRandomWord(), self.src_lang, self.dst_lang)
            elif args.chart_test_results:
                scores = self.dbHandler.getTestScores()
                self.xLearnView.plotLastTestResults(scores)
            elif args.translate:
                word_translation = self.translate.translate(self.__stripSpaces(args.translate), self.src_lang, self.dst_lang)
                self.xLearnView.printTranslation(self.__stripSpaces(args.translate), word_translation, self.src_lang, self.dst_lang)
            elif args.list:
                self.xLearnView.printListOfWordWithTrans(self.__getListWords(), self.src_lang, self.dst_lang)
            elif args.gui_random:
                sys.exit(self.xLearnView.printRandomTranslationWithGUI(self.__getRandomWord(), self.src_lang, self.dst_lang))
            elif args.test:
                words = self.__getListWords()
                random.shuffle(words)
                points = self.xLearnView.makeExam(words[:10], self.src_lang, self.dst_lang)
                self.__insertPoints(points)
            else:
                print('wrong comand ussage')
                parser.print_help(sys.stdout)
            if args.add:
                if word_translation != None:
                    self.__insertWords(args.translate, word_translation)
        except Exception as ex:
            print(ex)
            sys.exit(1)

    def __insertPoints(self, points):
        self.dbHandler.insertTestScore(points, self.src_lang, self.dst_lang)

    #strip spaces
    def __stripSpaces(self, word):
        return "".join(word.rstrip().lstrip())

    #get random word from with dbHanlder
    def __getRandomWord(self):
        return self.dbHandler.getRandomWord(self.src_lang)

    #insert word to database
    def __insertWords(self, word: str, translation: str):
        self.dbHandler.insertWords(word, translation, self.src_lang, self.dst_lang)

    #delete word from database
    def __deleteWords(self, words: list):
        for x in words:
            self.dbHandler.deleteWord(self.src_lang, x)

    #get list of words
    def __getListWords(self):
        return self.dbHandler.getWordsList(self.src_lang)

    # settup default languages from .config.json file
    def __setupDefaults(self):        
        with open(os.path.join(os.path.dirname(__file__), '.conf/.config.json')) as config_files:
            self.__config = json.load(config_files)

        self.src_lang = self.__config['default_source_language']
        self.dst_lang = self.__config['default_dest_language']