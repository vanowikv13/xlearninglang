from PyQt5 import QtWidgets, QtGui, QtCore

#Class for exams view
class ExamView:
    def __init__(self, words, src_lang, dst_lang):
        self.words = words
        self.src_lang = src_lang
        self.dst_lang = dst_lang
        self.max_points = len(words)
        self.points_scored = 0
        #list with references to textbox
        self.textboxes = []
        #list with labels
        self.labels = []
        #prepare items before printing them in GUI
        self.__setPreparedtest_items()
        #settup window to be started
        self.__setupGUIWindow()

    #run exam window
    def runExam(self):
        self.window.show()
        return self.app.exec_()

    #settup gui window basic
    def __setupGUIWindow(self):
        self.app = QtWidgets.QApplication([])
        self.window = QtWidgets.QWidget()
        self.window.setWindowTitle('Test: '+self.src_lang + ' => ' + self.dst_lang)
        self.window.setGeometry(600, 400, 500, 700)
        self.title_label = QtWidgets.QLabel(self.window)
        self.__setupGUIElement(self.title_label, 10, 10, 'Points: 0/'+str(self.max_points), 18, 490, QtCore.Qt.AlignCenter)
        t = 40
        for key in self.test_items:
            label = QtWidgets.QLabel(self.window)
            self.__setupGUIElement(label, 10, t, key, 14, 480)
            self.labels.append(label)

            intputField = QtWidgets.QLineEdit(self.window)
            self.__setupGUIElement(intputField, 10, t+25, '', 14, 480)
            self.textboxes.append(intputField)
            t+= 60

        self.button = QtWidgets.QPushButton(self.window)
        self.__setupGUIElement(self.button, 150, t+10, 'Finish test', 18, 200)
        self.button.clicked.connect(self.__finishExam)

    #when exam is finished print scored points
    def __finishExam(self):
        if self.points_scored != 0:
            self.points_scored = 0
        i = 0
        for key in self.test_items:
            self.labels[i].setText(key + " - " + ", ".join(self.test_items[key]))
            if self.textboxes[i].text().rstrip().lstrip().lower() in self.test_items[key]:
                self.points_scored += 1
                self.labels[i].setStyleSheet('color: green;')
                self.textboxes[i].setStyleSheet('color: white; background-color: green')
            else:
                self.labels[i].setStyleSheet('color: red;')
                self.textboxes[i].setStyleSheet('color: black; background-color: red')
            i += 1
            self.title_label.setText('Points:'+str(self.points_scored)+'/'+str(self.max_points))
        self.button.clicked.connect(QtWidgets.qApp.quit)
        self.button.setText("Dismiss!")

    #prepare items words for test
    def __setPreparedtest_items(self):
        self.test_items = {}
        for word in self.words:
            keys = list(word.keys())
            trans = []
            for x in word[keys[1]]:
                for key in x:
                    if x[key] == self.dst_lang:
                        trans.append(key.lower())
            if(len(trans) > 0):
                self.test_items[keys[1]] = trans.copy()

    #settup single GUI element like label, button ...
    def __setupGUIElement(self, element, xPos, yPos, text, fontSize, fixedWidth, alignment=None):
        element.setFont(QtGui.QFont("Arial", fontSize, QtGui.QFont.Bold))
        element.setText(text)
        element.move(xPos, yPos)
        element.setFixedWidth(fixedWidth)
        if alignment != None:
            element.setAlignment(alignment)
        return element