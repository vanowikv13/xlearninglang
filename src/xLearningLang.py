#!/usr/bin/env python
import sys
import argparse
from x_learning_lang import XLearnLang

def config_parser():
    #configure parser with each options
    parser = argparse.ArgumentParser(description="XLearningLanguageApp")
    parser.add_argument("-S", "--source_language", help='Specify source language for other options')
    parser.add_argument("-D", "--destination_language", help='Specify destination language for other options')
    parser.add_argument("-i", "--insert", nargs='+', help='Insert: word translations or Insert: "some sentence" "translated sentence" (only 2 arguments)')
    parser.add_argument("-t", "--translate" ,help='Translate: "sentence to translate"')
    parser.add_argument("-a", "--add", help='Save word with translation (works only with translation)', action='store_true')
    parser.add_argument("-T", "--test", help='Make test', action='store_true')
    parser.add_argument("-d", "--delete", nargs='+', help='Delete all occurrences of words from list: word1  word2 ...')
    parser.add_argument("-r", "--random", help='Get random word from list', action='store_true')
    parser.add_argument("-l", "--list", help='List actually learning words', action='store_true')
    parser.add_argument("-g", "--gui_random", help='Print random word in GUI', action='store_true')
    parser.add_argument("-c", "--chart_test_results", help='Chart last test results', action='store_true')
    return parser

def main():
    #Creating basic xLearnLangApp
    xLearnLangApp = XLearnLang()

    #settup parser
    parser = config_parser()

    #settup args
    args = parser.parse_args()

    #make action on specific args
    xLearnLangApp.actionOnArgs(args, parser)


if __name__ == '__main__':
    main()