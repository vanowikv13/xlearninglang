# xLearningLang
### Description:
Project let you translate from src language to dst language, save words and translation, delete them, insert words and translation and latter list them.

### Required packages
* mongodb
```bash
pip install pymongo googletrans rich PyQt5 pyinstaller
```

### Create executable with pyinstaller
To make executable use (depend of the system option: --hidden-import 'packaging.requirements', might be not needed):
```bash
pyinstaller xLearningLang.py --onefile --hidden-import 'packaging.requirements' --add-data ".conf/.*.json:.conf"
```

### Using PKGBUILD file
* Remember to have already installed mongodb
```bash
makepkg
```

### Some options usage:
* print help
```bash
xLearningLang -h
```

* translate from source to destination (if source and destination is not specified it will use default source and destination from .config file)
```bash
xLearningLang -S en -D pl -t "Translate me from english to polish"
```

* adding translated sentence to the list
```bash
xLearningLang -S en -D pl -t word --add
```

* delete word from english list (this do not delete translations of word)
```bash
xLearningLang -S en -d word
```

* insert word and translation (using defaults in .config if langs are not specified):
```bash
xLearningLang -i word słowo
```

* printing list of words and translation (using defaults in .config if langs are not specified)
```bash
xLearningLang -l
```

* print random word and translation (using defaults in .config if langs are not specified)
```
xLearningLang -r
```

* print random with GUI word and translation (using defaults in .config if langs are not specified)
```
xLearningLang -g
```

* make test with words using GUI (using defaults in .config if langs are not specified)
```
xLearningLang -T
```

* print char with last scores (using defaults in .config if langs are not specified)
```
xLearningLang -c
```